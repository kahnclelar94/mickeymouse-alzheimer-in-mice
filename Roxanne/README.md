# Single Cell Analysis of Cluster 17

Comparing the automatic annotation with our found clusters, we decided to study several clusters.

![](Figures/Featureplots_Cluster17/Umap_pred_vs_ours.jpeg)

Looking at the Heatmap we can see that cluster 17 and 19 share many similar genes. I decide it would be interesting to see if we can find Gene Features that would serve as Markers to differentaite between the two.

![](Figures/Featureplots_Cluster17/Heatmap.jpeg)

## Gene Features found for Cluster 17

I first used the FindAllMarkers() to do find out the most highly overexpressed Features in Cluster 17. GO Dotplot was not possible because the cell subsets were too small to analyse the gene ontology. The Features I found were sadly not conclusive for a Cell type as their expression was very widespread among the cell cluster, and as such do not serve well as Markers for Cluster 17. However, two of the Top 3 overexpressed genes are associated with Alzheimers in Literature (C4b belongs to the most overexpressed genes in AD vs control and Stard5 is a Stard6 SNP, which is neuroprotective. SNPs are vassociated with AD):

![](Figures/Featureplots_Cluster17/Cluster17_C4b.jpeg)

![](Figures/Featureplots_Cluster17/Cluster17_Stard5.jpeg)

![![](Figures/Featureplots_Cluster17/Cluster17_Necab12.jpeg)]()

## Gene Features Differentially Expressed in 17 vs 19

Since previous analysis were not conclusive I decided to make a differential expression comparison to see which genes are differently expressed in cluster 17 vs 19. However, again the search for a cell name was inconclusive.

Rem2 differentiates cells most 17 from 19, as does Plch2. However both give inconclusive results in the Cellmarker table. Plch2 is a retina marker in mouse, Rem2 is not found in the table or Literature.![](Figures/Featureplots_Cluster17/Cpne7.jpeg)![](Figures/Featureplots_Cluster17/Plch2.jpeg)![](Figures/Featureplots_Cluster17/Col11a1.jpeg)

![](Figures/Featureplots_Cluster17/Rem2.jpeg)

![](Figures/Featureplots_Cluster17/Thsd4.jpeg)
